﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using OxyPlot.Xamarin.Android;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace AppGraficosOxyPlot
{
    [Activity(Label = "AppGraficosOxyPlot", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        PlotView view;
        Button btnGraficoLinha;
        Button btnGraficoPizza;
        Button btnGraficoSenoide;
        Button btnGraficoBarras;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            view = FindViewById<PlotView>(Resource.Id.graficoView);
            btnGraficoLinha = FindViewById<Button>(Resource.Id.btnLinha);
            btnGraficoBarras = FindViewById<Button>(Resource.Id.btnBarras);
            btnGraficoSenoide = FindViewById<Button>(Resource.Id.btnSenoide);
            btnGraficoPizza = FindViewById<Button>(Resource.Id.btnPizza);

            btnGraficoBarras.Click += BtnGraficoBarras_Click;
            btnGraficoLinha.Click += BtnGraficoLinha_Click;
            btnGraficoPizza.Click += BtnGraficoPizza_Click;
            btnGraficoSenoide.Click += BtnGraficoSenoide_Click;
        }

        private enum TipoGrafico
        {
            Barras, Linhas, Senoide, Pizza
        }

        #region Eventos        
        private void BtnGraficoSenoide_Click(object sender, EventArgs e)
        {
            view.Model = Grafico(TipoGrafico.Senoide);
        }

        private void BtnGraficoPizza_Click(object sender, EventArgs e)
        {
            view.Model = Grafico(TipoGrafico.Pizza);
        }

        private void BtnGraficoLinha_Click(object sender, EventArgs e)
        {
            view.Model = Grafico(TipoGrafico.Linhas);
        }

        private void BtnGraficoBarras_Click(object sender, EventArgs e)
        {
            view.Model = Grafico(TipoGrafico.Barras);
        }
        #endregion

        #region Metodos
        private PlotModel Grafico(TipoGrafico tipoGrafico)
        {
            var plotModel = new PlotModel();
            
            switch (tipoGrafico)
            {
                case TipoGrafico.Barras:
                    {
                        plotModel.Title = "Vendas - Priemiro Semestre 2015/2016";
                        plotModel.LegendPlacement = LegendPlacement.Outside;
                        plotModel.LegendPosition = LegendPosition.BottomCenter;
                        plotModel.LegendOrientation = LegendOrientation.Horizontal;
                        plotModel.LegendBorderThickness = 0;

                        var series2015 = new BarSeries { Title = "2015", StrokeColor = OxyColors.Black, FillColor = OxyColors.DarkCyan, StrokeThickness = 1 };
                        series2015.Items.Add(new BarItem { Value = 25000 });
                        series2015.Items.Add(new BarItem { Value = 55000 });
                        series2015.Items.Add(new BarItem { Value = 35000 });
                        series2015.Items.Add(new BarItem { Value = 15000 });
                        series2015.Items.Add(new BarItem { Value = 85000 });
                        series2015.Items.Add(new BarItem { Value = 125000 });

                        var series2016 = new BarSeries { Title = "2016", Background = OxyColors.Aquamarine, StrokeColor = OxyColors.Azure, StrokeThickness = 1 };
                        series2016.Items.Add(new BarItem { Value = 27500 });
                        series2016.Items.Add(new BarItem { Value = 47500 });
                        series2016.Items.Add(new BarItem { Value = 47500 });
                        series2016.Items.Add(new BarItem { Value = 17500 });
                        series2016.Items.Add(new BarItem { Value = 97500 });
                        series2016.Items.Add(new BarItem { Value = 127500 });

                        var meses = new CategoryAxis { Position = AxisPosition.Left };
                        meses.Labels.Add("JAN");
                        meses.Labels.Add("FEV");
                        meses.Labels.Add("MAR");
                        meses.Labels.Add("ABR");
                        meses.Labels.Add("MAI");
                        meses.Labels.Add("JUN");

                        var valueAxis = new LinearAxis
                        {
                            Position = AxisPosition.Bottom,
                            Minimum = 0,
                            Maximum = 0.06,
                            AbsoluteMinimum = 0
                        };                        

                        plotModel.Series.Add(series2015);
                        plotModel.Series.Add(series2016);

                        plotModel.Axes.Add(meses);
                        plotModel.Axes.Add(valueAxis);
                    }
                    break;
                case TipoGrafico.Linhas:
                    {
                        plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
                        plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Maximum = 10, Minimum = 0 });
                        plotModel.Title = "Variação do Dolar 2016";
                        var series = new LineSeries();
                        series.Color = OxyColors.Yellow;
                        series.MarkerType = MarkerType.Star;
                        series.MarkerSize = 5;
                        series.MarkerStroke = OxyColors.White;
                        series.MarkerFill = OxyColors.Black;
                        series.MarkerStrokeThickness = 4.0;

                        series.Points.Add(new DataPoint(0.0, 6.0));
                        series.Points.Add(new DataPoint(1.4, 2.1));
                        series.Points.Add(new DataPoint(2.0, 4.2));
                        series.Points.Add(new DataPoint(3.3, 2.3));
                        series.Points.Add(new DataPoint(4.7, 7.4));
                        series.Points.Add(new DataPoint(6.0, 6.2));
                        series.Points.Add(new DataPoint(8.9, 8.9));
                        plotModel.Series.Add(series);
                    }
                    break;
                case TipoGrafico.Senoide:
                    {
                        plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom });
                        plotModel.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Maximum = 10, Minimum = 0 });
                        plotModel.Title = "Tremores Terrestres";
                        var series = new LineSeries();
                        series.Color = OxyColors.DarkBlue;
                        series.MarkerType = MarkerType.Circle;
                        series.MarkerSize = 3;
                        series.MarkerStroke = OxyColors.Green;
                        series.MarkerStrokeThickness = 1.0;

                        int contador = 10;
                        for (double index_X = -10; index_X < 10; index_X += 0.4)
                        {
                            double index_Y = 0;
                            for (int increment = 0; increment < contador; increment++)
                            {
                                int indexCount = 1 * 2 + 1;
                                index_Y += Math.Sin(indexCount * index_X * 0.1);
                            }
                            series.Points.Add(new DataPoint(index_X, index_Y));
                        }
                        plotModel.Series.Add(series);
                    }
                    break;
                case TipoGrafico.Pizza:
                    {
                        plotModel.Title = "Poupulação Continente";
                        var series = new PieSeries();
                        series.StrokeThickness = 2.0;
                        series.InsideLabelPosition = 0.8;
                        series.AngleSpan = 360;
                        series.StartAngle = 0;

                        series.Slices.Add(new PieSlice("Africa", 5000) { IsExploded = false, Fill = OxyColors.PaleTurquoise });
                        series.Slices.Add(new PieSlice("Americas", 3000) { IsExploded = true });
                        series.Slices.Add(new PieSlice("Asia", 4000) { IsExploded = true });
                        series.Slices.Add(new PieSlice("Europa", 2000) { IsExploded = true });
                        series.Slices.Add(new PieSlice("Oceania", 1500) { IsExploded = true });                        
                        plotModel.Series.Add(series);
                    }
                    break;
                default:
                    break;
            }

            return plotModel;
        }
        #endregion
    }
}

